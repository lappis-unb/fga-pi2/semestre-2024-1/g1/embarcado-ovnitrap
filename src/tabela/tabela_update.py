import json
import os
from datetime import datetime

# Define file names
file_names = [
    'id.txt',
    'picture_id.txt',
    'picture_uv_id.txt',
    'water_level.txt',
    'water_temperature.txt',
    'battery_level.txt',
    'trap_activity.txt',
    'time_of_report.txt',
    'report_count.txt'
]

# Define column names corresponding to the file names
column_names = [
    'id',
    'picture_id',
    'picture_uv_id',
    'water_level',
    'water_temperature',
    'battery_level',
    'trap_activity',
    'time_of_report',
    'report_count'
]

# Function to read data from a file and return it as a list of strings
def read_file(file_name):
    with open(file_name, 'r') as file:
        return file.read().splitlines()

# Load existing data from JSON file if it exists
json_file_name = 'table.json'
if os.path.exists(json_file_name):
    with open(json_file_name, 'r') as json_file:
        table = json.load(json_file)
else:
    table = []

# Read data from each file and store it in a dictionary
data = {column: read_file(file_name) for column, file_name in zip(column_names, file_names)}

# Determine the number of rows (assuming all files have the same number of lines)
num_rows = len(data['id'])

# Get the current time
current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Create the new row (using the last entry from each file for the new data)
new_row = {column: data[column][-1] for column in column_names}
new_row['time_of_report'] = current_time  # Update time_of_report to current time

# Append the new row to the table
table.append(new_row)

# Convert the table to a JSON string and write it to the JSON file
with open(json_file_name, 'w') as json_file:
    json.dump(table, json_file, indent=4)

# Print the updated table
print(json.dumps(table, indent=4))
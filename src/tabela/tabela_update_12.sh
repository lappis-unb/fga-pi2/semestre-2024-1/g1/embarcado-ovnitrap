#!/bin/bash

# Directory containing .txt files
DIRECTORY="/home/bgbc1/Tabela"

# Existing JSON file
OUTPUT_FILE="output.json"

# Read the existing JSON content
if [ -f "$OUTPUT_FILE" ]; then
  json_content=$(cat "$OUTPUT_FILE")
else
  json_content="[]"
fi

# Initialize a new row object
new_row="{"

# Iterate over each .txt file in the directory
for file in "$DIRECTORY"/*.txt; do
  if [ -f "$file" ]; then
    # Read the first line of the file
    first_line=$(head -n 1 "$file")
    
    # Escape any double quotes in the first line
    first_line_escaped=$(echo "$first_line" | sed 's/"/\\"/g')
    
    # Add the first line to the new row object
    new_row+="\"$(basename "$file")\":\"$first_line_escaped\","
  fi
done

# Remove the trailing comma if the row is not empty
new_row="${new_row%,}"

# Close the new row object
new_row+="}"

# Add the new row to the existing JSON array
if [ "$json_content" != "[]" ]; then
  # Remove the closing bracket from existing content and append the new row
  combined_content="${json_content%]},""$new_row]"
else
  combined_content="[$new_row]"
fi

# Write the combined content back to the output file
echo "$combined_content" > "$OUTPUT_FILE"

echo "JSON data updated in $OUTPUT_FILE"
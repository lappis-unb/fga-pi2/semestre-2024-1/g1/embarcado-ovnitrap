#!/bin/bash

# Path to the JSON file
JSON_FILE="/home/bgbc1/Tabela/output.json"

# Path to the script to be executed
SCRIPT_TO_EXECUTE="tabela_update_13_correto.sh"

# Get the current time (adjust format as needed, here it's YYYY-MM-DD HH:MM:SS)
current_time=$(date +"%Y-%m-%d %H:%M")


# Read the last array from the JSON file
last_array=$(jq -r '.[-1]' "$JSON_FILE")

# Extract the second element of the last array
second_element=$(echo "$last_array" | jq -r '.[1]')

# Compare the second element with the current time
if [ "$second_element" != "$current_time" ]; then
  echo "Time has changed. Executing the script."
  echo "$current_time"
  echo "$second_element"
  # Execute the other script
  ./$SCRIPT_TO_EXECUTE
else
  echo "Time is the same. No action taken."
fi

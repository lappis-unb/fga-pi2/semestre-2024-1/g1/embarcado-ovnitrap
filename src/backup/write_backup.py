
def salva_files(dados,filename):
    print('&&&& SEM INTERNET - SALVANDO ARQUIVOS &&&&&')
        # Read existing data from the JSON file
    try:
        with open(filename, 'r') as json_file:
            data = json.load(json_file)
    except FileNotFoundError:
        data = []
    
    # Ensure the data read is a list
    if not isinstance(data, list):
        data = []

    # Append the new data to the existing data
    data.append(dados)

    # Write the updated data back to the JSON file
    try:
        with open(filename, 'w') as json_file:
            json.dump(data, json_file, indent=4)
        print(f"Data successfully written to {filename}")
    except Exception as e:
        print(f"An error occurred while writing to {filename}: {e}")

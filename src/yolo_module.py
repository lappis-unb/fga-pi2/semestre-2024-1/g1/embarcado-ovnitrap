import subprocess
import re
import os


def processamento_imagem():
    yolo_path = "src/yolov5"
    input_image = "captured_image.jpg"

    command = f"python3 {yolo_path}/detect.py --weights {yolo_path}/runs/train/yolov5s_results/weights/best.pt --img 640 --conf 0.4 --source {yolo_path}/{input_image}"

    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    output = result.stderr

    if "(no detections)" in output:
        num_larvas = "0"
    else:
        num_larvas_match = re.search(r"(\d+) larva[s]?", output)
        num_larvas = num_larvas_match.group(1) if num_larvas_match else "Não encontrado"

    # Extracting directory where results are saved
    dir_imagem_match = re.search(r"Results saved to \x1b\[1m(.+?)\x1b\[0m", output)
    dir_imagem = dir_imagem_match.group(1) if dir_imagem_match else "Não encontrado"

    # Including the name of the input file in the directory path
    saved_image_path = (
        os.path.join(dir_imagem, os.path.basename(input_image))
        if dir_imagem != "Não encontrado"
        else "Não encontrado"
    )

    print(f"Número de larvas: {num_larvas}")
    print(f"Caminho da imagem salva: {saved_image_path}")

    return num_larvas

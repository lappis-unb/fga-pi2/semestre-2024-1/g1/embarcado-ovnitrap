#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
    // Check if image path is provided
    if (argc != 2) {
        cout << "Usage: ./focus_checker <image_path>" << endl;
        return -1;
    }

    // Read the image
    Mat image = imread(argv[1]);
    if (image.empty()) {
        cout << "Could not open or find the image" << endl;
        return -1;
    }

    // Convert image to grayscale
    Mat grayImage;
    cvtColor(image, grayImage, COLOR_BGR2GRAY);

    // Calculate the variance of Laplacian
    Mat laplacianImage;
    Laplacian(grayImage, laplacianImage, CV_64F);
    Scalar mean, stddev;
    meanStdDev(laplacianImage, mean, stddev);
    double focusMeasure = stddev.val[0] * stddev.val[0];

    // Define a threshold for determining focus
    double threshold = 100.0; // You may adjust this threshold based on your requirements

    // Check if the image is in focus or not
    if (focusMeasure < threshold) {
        cout << "Image is blurred (focus measure: " << focusMeasure << ")" << endl;
    } else {
        cout << "Image is in focus (focus measure: " << focusMeasure << ")" << endl;
    }

    return 0;
}
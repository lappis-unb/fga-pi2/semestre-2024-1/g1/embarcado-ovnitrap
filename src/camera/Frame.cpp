#include <iostream>
#include <opencv2/opencv.hpp>

int main(int argc, char* argv[]) {
    std::cout << "0";

    cv::VideoCapture cap(0);  // Open the default camera (index 0)

    if (!cap.isOpened()) {
        std::cout << "Failed to open the camera." << std::endl;
        return -1;
    }

    cv::Mat frame;
    cap >> frame;  // Capture a frame

    std::string directory = "./src/";
    std::string filename = directory + argv[1];  // File name to save the frame
    std::cout << "Frame captured and saved as " << filename << std::endl;
    
    cv::imwrite(filename, frame);  // Save the frame to a file


    cap.release();  // Release the camera

    return 0;
}

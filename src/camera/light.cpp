#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
    // Check if image path is provided
    if (argc != 2) {
        cout << "Usage: ./light_level_checker <image_path>" << endl;
        return -1;
    }

    // Read the image
    Mat image = imread(argv[1]);
    if (image.empty()) {
        cout << "Could not open or find the image" << endl;
        return -1;
    }

    // Convert image to grayscale
    Mat grayImage;
    cvtColor(image, grayImage, COLOR_BGR2GRAY);

    // Calculate average pixel intensity (brightness)
    Scalar meanIntensity = mean(grayImage);

    // Get the average intensity value
    double lightLevel = meanIntensity.val[0];

    // Define a threshold for determining light level
    double threshold = 127.0; // You may adjust this threshold based on your requirements

    // Check if the light level is low or high
    if (lightLevel < threshold) {
        cout << "Low light level (average intensity: " << lightLevel << ")" << endl;
    } else {
        cout << "High light level (average intensity: " << lightLevel << ")" << endl;
    }

    return 0;
}
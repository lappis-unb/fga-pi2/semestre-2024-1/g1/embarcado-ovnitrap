import RPi.GPIO as GPIO
from sensores.sensor_temp import read_temp
import time
import subprocess
from service.base_service import atualiza_servidor
import datetime
from roboflow import Roboflow
import supervision as sv
import cv2
from PIL import Image  

RELE_PIN_LED = 16
RELE_PIN_NIVEL_AGUA = 23
RELE_PIN_BOMBA = 22

rf = Roboflow(api_key="CBj9VGjS4fXh3KQ7CrB4")
project = rf.workspace().project("ovnitrap")
model = project.version(3).model

class Armadilha:
    def __init__(self,
                 id_armadilha,
                 temperatura=0.0,
                 nivel_agua=0.0,
                 conexao=False,
                 nivel_atividade_larval=0.0,
                 ultima_descarga = datetime.datetime.now()):

        self.temperatura = temperatura
        self.nivel_agua = nivel_agua
        self.conexao = conexao
        self.nivel_atividade_larval = nivel_atividade_larval
        self.id_armadilha = id_armadilha
        self.ultima_descarga = ultima_descarga

    def setup(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(RELE_PIN_LED, GPIO.OUT)
        GPIO.setup(RELE_PIN_NIVEL_AGUA, GPIO.IN)
        GPIO.setup(RELE_PIN_BOMBA, GPIO.OUT)
        time.sleep(2)

    def liga_led(self):
        GPIO.output(RELE_PIN_LED, GPIO.HIGH)

    def desliga_led(self):
        GPIO.output(RELE_PIN_LED, GPIO.LOW)

    def get_temperatura(self):
        self.temperatura = read_temp()
        print('Temperatura da água: ', self.temperatura)
        return self.temperatura

    def get_nivel_agua(self):
        if GPIO.input(RELE_PIN_NIVEL_AGUA):  # Se o pino estiver em HIGH (3.3V)
            print("Nível da água: Baixo")
            self.nivel_agua = 0

            return self.nivel_agua
        else:  # Se o pino estiver em LOW (0V)
            print("Nível da água: Alto")
            self.nivel_agua = 1
            return self.nivel_agua

    def liga_bomba(self):
        GPIO.output(RELE_PIN_BOMBA, GPIO.HIGH)

    def desliga_bomba(self):
        GPIO.output(RELE_PIN_BOMBA, GPIO.LOW)

    def run_C_exec(self,program_name):
        try:
            subprocess.run([program_name, f"captured_image.jpg"], check=True)
        except subprocess.CalledProcessError as e:
            print(f"Erro ao executar {program_name}: {e}")

    def tira_foto(self):
        print("tirando foto...")
        self.run_C_exec('./src/camera/frame.out')

    def close_connection(self):
        self.conexao = False
        GPIO.cleanup()

    def enche_reservatorio(self):
        print('ENCHENDO RESERVATÓRIO')
        GPIO.output(RELE_PIN_BOMBA, GPIO.HIGH)
        time.sleep(20)
        GPIO.output(RELE_PIN_BOMBA, GPIO.LOW)

    def esvaziar(self):
        print('ESVAZIANDO')
        GPIO.output(RELE_PIN_BOMBA, GPIO.HIGH)
        time.sleep(10)
        GPIO.output(RELE_PIN_BOMBA, GPIO.LOW)

    def da_descarga(self):
        self.ultima_descarga = datetime.datetime.now()
        self.esvaziar()
        time.sleep(15)
        self.enche_reservatorio()
        time.sleep(3)
        self.esvaziar()
        time.sleep(15)
        self.enche_reservatorio()

    def get_relatorio(self):
        return {
            "id_armadilha": self.id_armadilha,
            "temperatura": self.temperatura,
            "nivel_agua": self.nivel_agua,
            "conexao": self.conexao,
            "nivel_atividade_larval": self.nivel_atividade_larval
        }

    def atualiza_servidor_dados(self,qtd_larva):
        dados = {
            'armadilha': self.id_armadilha,
            'temperatura': round(self.temperatura,2), # OK
            'nivel_bateria': 1, # NOTOK
            'nivel_agua': self.nivel_agua, # OK
            'latitude': -23.550520, # NOTOK
            'longitude': -46.633308, # NOTOK
            'qtd_larvas': qtd_larva, # NOTOK
            'data_hora': datetime.datetime.now().isoformat(),
            'data_hora_descarga': self.ultima_descarga.isoformat()
        }
        atualiza_servidor(dados)

    def processa_foto(self):
        print("processando foto...")
        result = model.predict("./src/captured_image.jpg", confidence=70, overlap=30).json()

        labels = [item["class"] for item in result["predictions"]]
        print('\n\nlabels', labels)
        detections = sv.Detections.from_roboflow(result)
        print(detections)
        label_annotator = sv.LabelAnnotator()
        bounding_box_annotator = sv.BoxAnnotator()

        image = cv2.imread("./src/captured_image.jpg")

        annotated_image = bounding_box_annotator.annotate(
            scene=image, detections=detections)
        
        annotated_image = label_annotator.annotate(
            scene=annotated_image, detections=detections, labels=labels)
    
        # Salvar annotated_image em captured_image.jpg
        im = Image.fromarray(annotated_image)
        im.save("./src/captured_image.jpg")

        return len(labels)

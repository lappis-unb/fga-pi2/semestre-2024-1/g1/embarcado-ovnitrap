import cv2

# Caminho da imagem de entrada e saída
caminho_imagem_entrada = '02.jpg'
caminho_imagem_saida = '02_escala_de_cinza.jpg'

# Carregar a imagem
imagem = cv2.imread(caminho_imagem_entrada)

# Converter para escala de cinza
imagem_gray = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

# Salvar a imagem em escala de cinza
cv2.imwrite(caminho_imagem_saida, imagem_gray)

print(f'Imagem em escala de cinza salva em: {caminho_imagem_saida}')

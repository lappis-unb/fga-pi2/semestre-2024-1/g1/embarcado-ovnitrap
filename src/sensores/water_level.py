import RPi.GPIO as GPIO


def mede_nivel_agua():
    sensor_pin = 23
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(sensor_pin, GPIO.IN)
    if GPIO.input(sensor_pin):  # Se o pino estiver em HIGH (3.3V)
        print("Nível da água: Baixo")
        return 0
    else:  # Se o pino estiver em LOW (0V)
        print("Nível da água: Alto")
        return 1
    GPIO.cleanup()
    
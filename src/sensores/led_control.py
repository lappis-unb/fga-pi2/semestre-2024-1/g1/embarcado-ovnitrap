"""Executando o código
python3 led_control.py

"""

# OK

import RPi.GPIO as GPIO
import time

# Definir os pinos GPIO
led_pins = 17
# Configurar o modo da numeração dos pinos
GPIO.setmode(GPIO.BCM)

# Configurar os pinos GPIO como saída
GPIO.setup(led_pins, GPIO.OUT)

try:
    # Acionar todos os LEDs
    GPIO.output(led_pins, GPIO.HIGH)
    
    # Manter os LEDs ligados por 10 segundos
    time.sleep(1)

    GPIO.output(led_pins, GPIO.LOW)

    time.sleep(1)

    GPIO.output(led_pins, GPIO.HIGH)

    time.sleep(1)

    GPIO.output(led_pins, GPIO.LOW)

finally:
    # Desligar todos os LEDs e limpar a configuração GPIO
    GPIO.output(led_pins, GPIO.LOW)
    GPIO.cleanup()

# módulo de leitura dos sensores
# to do 
# importar funções dos arquivos e realizar a leitura

from .sensor_temp import read_temp
from .water_level import mede_nivel_agua

def mede_sensores():
    sensores = {
        'temperatura': 0,
        'nivel_agua': -1,
    } 

    sensores['temperatura'] = read_temp()
    sensores['nivel_agua'] = mede_nivel_agua()
    
    print(sensores)
    return sensores
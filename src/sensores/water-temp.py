import os
import glob
import time
import RPi.GPIO as GPIO

# Define o pino do sensor DS18B20
sensor_pin = 4

# Configuração inicial do GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor_pin, GPIO.IN)

# Função para ler a temperatura do sensor DS18B20
def read_temp_raw():
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + 'w1_bus_master1')[0]
    device_file = device_folder + '/w1_master_slaves'
    with open(device_file, 'r') as f:
        lines = f.readlines()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
        print(lines)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        print(temp_c)
        return temp_c

# try:
#     temperature = read_temp()
#     print(f'Temperatura da água: {temperature:.1f} °C')

# except Exception as e:
#     print(f"Erro ao ler temperatura: {str(e)}")

# finally:
#     GPIO.cleanup()

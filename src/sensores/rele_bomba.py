import RPi.GPIO as GPIO
import time

# Configuração dos pinos

# Configuração da biblioteca RPi.GPIO

def enche_reservatorio():
    print('ENCHENDO RESERVATÓRIO')
    GPIO.output(RELE_PIN, GPIO.HIGH)
    time.sleep(13)
    GPIO.output(RELE_PIN, GPIO.LOW)

def esvaziar():
    print('ESVAZIANDO')
    GPIO.output(RELE_PIN, GPIO.HIGH)
    time.sleep(6)

def da_descarga():
    esvaziar()
    time.sleep(12)
    enche_reservatorio()
    time.sleep(3)
    esvaziar()
    time.sleep(12)
    enche_reservatorio()
    GPIO.output(RELE_PIN, GPIO.LOW)

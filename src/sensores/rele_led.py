import RPi.GPIO as GPIO
import time

# Configuração dos pinos


def liga_led():
    RELE_PIN = 16  # Altere para o pino GPIO que você está usando

    # Configuração da biblioteca RPi.GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(RELE_PIN, GPIO.OUT)
    GPIO.output(RELE_PIN, GPIO.HIGH)

def desliga_led():
    RELE_PIN = 16  # Altere para o pino GPIO que você está usando

    # Configuração da biblioteca RPi.GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(RELE_PIN, GPIO.OUT)
    GPIO.output(RELE_PIN, GPIO.LOW)
    GPIO.cleanup()
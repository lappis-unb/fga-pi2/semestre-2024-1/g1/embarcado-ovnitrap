import time
import threading
from armadilha import Armadilha

running = True


def menu(armadilha: Armadilha):
    global running

    while running:
        print("\nEscolha uma das opções abaixo:\n")
        print("1) Ligar LED")
        print("2) Desligar LED")
        print("3) Dá descarga")
        print("4) Encher Reservatório")
        print("5) Esvaziar Reservatório")
        print("6) Tirar Foto")
        print("7) Processar Foto")
        print("8) Enviar Dados")
        print("9) Verificar Temperatura")
        print("10) Ligar bomba")
        print("11) Desliga bomba")
        print("12) Medir nivel de agua")
        print("13) Sair")

        try:
            opcao = int(input("\nDigite o número da opção desejada: "))

            if opcao == 1:
                armadilha.liga_led()
            elif opcao == 2:
                armadilha.desliga_led()
            elif opcao == 3:
                armadilha.da_descarga()
            elif opcao == 4:
                armadilha.enche_reservatorio()
            elif opcao == 5:
                armadilha.esvaziar()
            elif opcao == 6:
                armadilha.tira_foto()
            elif opcao == 7:
                print(armadilha.processa_foto())
            elif opcao == 8:
                armadilha.atualiza_servidor_dados()
            elif opcao == 9:
                print(armadilha.get_temperatura())
            elif opcao == 10:
                armadilha.liga_bomba()
            elif opcao == 11:
                armadilha.desliga_bomba()
            elif opcao == 12:
                armadilha.get_nivel_agua()
            elif opcao == 13:
                print("Saindo...")
                armadilha.close_connection()
                break
            else:
                print("Opção inválida! Tente novamente.")
        except ValueError:
            print("Entrada inválida! Por favor, digite um número.")


def main():
    global running
    print("Ligando...")
    armadilha = Armadilha(id_armadilha="DQEDA")
    armadilha.setup()

    interface = threading.Thread(target=menu, args=(armadilha,))
    interface.start()

    try:
        while running:
            armadilha.liga_led()
            armadilha.tira_foto()
            armadilha.desliga_led()

            qtd_larva = armadilha.processa_foto()

            armadilha.get_temperatura()
            armadilha.get_nivel_agua()
            armadilha.atualiza_servidor_dados(qtd_larva)

            time.sleep(60)

    except KeyboardInterrupt:
        print("Saindo do programa...")
        time.sleep(1)
    finally:
        running = False
        interface.join()
        armadilha.close_connection()
        time.sleep(1)


if __name__ == "__main__":
    main()

import requests
import json
import os

URL = 'https://backend-br8t.onrender.com/api/'

headers = {
    'Content-Type': 'application/json',
}

def atualiza_servidor(body):
    script_dir = os.path.dirname(__file__)
    rel_path = "../"
    abs_file_path = os.path.join(script_dir, rel_path)
    current_file ="captured_image.jpg"
    image = open(abs_file_path+current_file,'rb')
    print(image)
    files = {
        'imagem': image
    }

    try:
        with open('./src/backup/data.json', 'r') as json_file:
            data = json.load(json_file)
    except FileNotFoundError:
        data = []
    
    if len(data) > 0:
        for entry in data:
            response = requests.post(URL + 'relatorios/', data=entry, files=files)
            # Checking the response
            if response.status_code == 201:
                print('Success:', response.json())
            else:
                print('Failed:', response.status_code, response.json())

    print('FAZENDO REQUISIÇÃO')
    # Making the POST request
    print(body)
    response = requests.post(URL + 'relatorios/', data=body, files=files)

    # Checking the response
    if response.status_code == 201:
        print('Success:', response.json())
    else:
        print('Failed:', response.status_code, response.json())

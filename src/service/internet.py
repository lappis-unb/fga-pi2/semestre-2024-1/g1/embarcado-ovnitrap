import subprocess
import time

def check_internet():
    url = "http://www.google.com"
    command = ["wget", "--spider", url]

    print("### CHECKING INTERNET ###")

    for attempt in range(5):
        try:
            response = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if response.returncode == 0:
                print(f"Attempt {attempt + 1}: Internet is available.")
                return True
            else:
                print(f"Attempt {attempt + 1}: Internet is not available.")
        except Exception as e:
            print(f"Attempt {attempt + 1}: An error occurred: {e}")

        time.sleep(3)
    
    print("Internet is not available after 5 attempts.")
    return False

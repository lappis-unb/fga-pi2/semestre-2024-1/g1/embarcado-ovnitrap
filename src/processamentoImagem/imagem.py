import subprocess
import time
import datetime
from yolo_module import processamento_imagem

from os.path import dirname,abspath
d = dirname(dirname(abspath(__file__)))
import sys
sys.path.append(d)

def run_C_exec(program_name):
    try:
        subprocess.run([program_name, f"captured_image.jpg"], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Erro ao executar {program_name}: {e}")

def tira_foto():
    time.sleep(1)
    run_C_exec("./src/camera/frame.out")
    time.sleep(1)

    # qtd_larvas, img = processamento_imagem()
    qtd_larvas = processamento_imagem()

    # return {'foto':img,"data_hora": datetime.datetime.now(), 'quantidade_larvas':qtd_larvas}
    return {"data_hora": datetime.datetime.now(), 'quantidade_larvas':5}

def processa_imagem():
    try:
        print('Tirando foto...')
        imagem = tira_foto()
        return imagem
    except:
        pass
    finally:
        pass
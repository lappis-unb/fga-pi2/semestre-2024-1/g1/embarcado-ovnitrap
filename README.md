# Fluxograma do projeto

![fluxograma](./assets/fluxograma%20-%20ovnitrap%20-%20Frame%201.jpg)

O fluxo grama acime demonstra como será o fluxo de execução da armadilha. Assim que a armadilha for ligada o arquivo "laucher.sh" será executado atualizando a data e hora e chamando o arquivo "main.py".

## Camera

Para gerar um executável dos arquivos necessários para ter acesso a câmera, é necessário compilar da seguindo forma:

| $ g++ -o frame.out Frame.cpp `pkg-config --cflags --libs opencv4`

É necessário permisão para os arquivos na pasta camera:

| $ chmod +x /camera

**+x:** É a opção que indica que você está adicionando permissão de execução ao arquivo especificado. Nesse caso a pasta tem permissão para executar o arquivo pelo sistema operacional.

para executar em ambiente de desenvolvimento execute:

| $ bash launcher.sh
#!/bin/bash

# sudo ntpdate -u pool.ntp.org # atualizar data e hora

# Ativar o ambiente virtual
# python3 -m venv venv
source venv/bin/activate

# Instalar dependências (caso existam, ajustar conforme necessário)
# pip install -r ./src/requirements.txt

# Caminho do script Python
SCRIPT="./src/main.py"

# Executar o script Python
python3 "$SCRIPT"

# Desativar o ambiente virtual
deactivate
